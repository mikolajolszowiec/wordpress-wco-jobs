<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Wco_Jobs
 * @subpackage Wco_Jobs/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Wco_Jobs
 * @subpackage Wco_Jobs/admin
 * @author     Mikołaj Olszowiec <mikolajolszowiec@gmail.com>
 */
class Wco_Jobs_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Plugin_Name_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Plugin_Name_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style(  'wco-jobs-admin-datepicker-style', plugin_dir_url( __FILE__ ) . 'css/datepicker.css', array(), $this->version, false );
		wp_enqueue_style(  'wco-jobs-admin-style', plugin_dir_url( __FILE__ ) . 'css/wco-jobs-admin.css', array(), $this->version, false );
	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Plugin_Name_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Plugin_Name_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
                if( !wp_script_is('jquery-ui-datepicker','enquequed')){
                    wp_enqueue_script( 'datepicker-script', plugin_dir_url( __FILE__ ) . 'js/datepicker.min.js', array( 'jquery' ), $this->version, false );
                }
                wp_enqueue_script( 'wco-jobs-admin-script', plugin_dir_url( __FILE__ ) . 'js/wco-jobs-admin.js', array('datepicker-script'), $this->version, false );
	}

}
