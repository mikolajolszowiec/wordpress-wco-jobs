<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


add_action( 'init', 'register_wco_jobs' );
function register_wco_jobs(){
	$args = array();
	
	$labels = array(
            'name'               => _x( 'Oferty pracy', 'post type general name' ),
            'singular_name'      => _x( 'Oferty pracy', 'post type singular name' ),
            'add_new'            => _x( 'Dodaj nową', 'Wco Jobs' ),
            'add_new_item'       => __( 'Dodaj nową ofertę pracy' ),
            'edit_item'          => __( 'Edytuj ofertę pracy' ),
            'new_item'           => __( 'Nowa praca' ),
            'all_items'          => __( 'Wszystkie oferty' ),
            'view_item'          => __( 'Pokaż wszystkie oferty' ),
            'search_items'       => __( 'Szukaj ofert pracy' ),
            'not_found'          => __( 'Nie znaleziono ofert pracy' ),
            'not_found_in_trash' => __( 'Nie znaleziono ofert pracy w koszu' ), 
            'parent_item_colon'  => __( 'Wco Jobs:', 'wco_jobs-textdomain' ),
            'settings'           => _x( 'Ustawienia:','wco_jobs-settings'),
            'menu_name'          => 'Oferty pracy'
        );
        $args = array(
            'labels' => $labels,
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'query_var' => true,
            //'menu_icon' => get_stylesheet_directory_uri() . '/article16.png',
            'rewrite' => true,
            'capability_type' => 'post',
            'has_archive' => 'job',
            'hierarchical' => false,
            'menu_position' => null,
            'supports' => array('title','editor','excerpt')
        );
        register_post_type( 'wco_jobs', $args ); 
}

add_action( 'init', 'register_taxonomies_wco_jobs', 0 );
function register_taxonomies_wco_jobs(){
	$texonomies_args = array();
	$labels = array(
            'name'              => _x( 'Kategorie ofert pracy', 'taxonomy general name' ),
            'singular_name'     => _x( 'Kategorie ofert pracy', 'taxonomy singular name' ),
            'search_items'      => __( 'Szukaj kategorii ofert pracy' ),
            'all_items'         => __( 'Wszystkie kategorie ofert pracy' ),
            'parent_item'       => __( 'Nadrzędne kategorie ofert pracy' ),
            'parent_item_colon' => __( 'Nadrzędne kategoria ofert pracy:' ),
            'edit_item'         => __( 'Edytuj kategorie ofert pracy' ), 
            'update_item'       => __( 'Aktualizuj kategorie ofert pracy' ),
            'add_new_item'      => __( 'Dodaj nową kategorię ofert pracy' ),
            'new_item_name'     => __( 'Nowa kategoria ofert pracy' ),
            'menu_name'         => __( 'Kategorie ofert pracy' ),
        );
        $texonomies_args = array(
            'labels' => $labels,
            'hierarchical' => true,
            'slug' => 'wco_jobs_category',
            'query_var' => true,
            'rewrite' =>true
        );
       // if(!taxonomy_exists('wco_jobs_category')){die ('taxonomy dziala ');
            register_taxonomy( 'wco_jobs_category', 'wco_jobs', $texonomies_args );
        //}
}

// add default category
add_action( 'init', 'set_category_wco_jobs' );
function set_category_wco_jobs() {
//    die("<h1>zostalem wywolany</h1>");
	if(!term_exists('Oferty pracy WCO', 'wco_jobs_category')){die ('term dziala');
            wp_insert_term(
                    'Oferty pracy WCO',
                    'wco_jobs_category',
                    array(
                      'description'	=> 'To jest domyślna kategoria dla ofert pracy.',
                      'slug' => 'wco_jobs_category'
                    )
            );
        }
}

function wco_jobs_settings() {
	add_submenu_page( 'edit.php?post_type=wco_jobs', __( 'Wco Jobs', 'wco_jobs' ), __( 'Ustawienia', 'wco_jobs' ), 'read_private_pages', 'wco_jobs_settings_page', 'wco_jobs_settings_page' );
}
function wco_jobs_settings_page(){
	include_once(WCOJOBS_DIR_PATH.'wco_jobs_settings_page.php');
}
add_action( 'admin_menu', 'wco_jobs_settings' );


add_action( 'add_meta_boxes', 'date_meta_boxes' );
function date_meta_boxes(){
	add_meta_box( 
        'wco_jobs_publication_term',
        'Czas publikacji oferty',
        'metabox_date_content',
        'wco_jobs',
        'normal',
        'high'
    );
}

function metabox_date_content( $post ) {
	$wco_jobs_publication_start = get_post_meta( get_the_ID(), 'wco_jobs_publication_start', true );
	$wco_jobs_publication_end = get_post_meta( get_the_ID(), 'wco_jobs_publication_end', true );
        echo '<label for="wco_jobs_publication_term"></label>';
        if(!$wco_jobs_publication_start) $wco_jobs_publication_start = date('Y-m-d', time());
        if(!$wco_jobs_publication_end) $wco_jobs_publication_end = date('Y-m-d', strtotime('14 days'));
        echo '<a>Od: </a><input type="text" id="wco_jobs_publication_start" name="wco_jobs_publication_start" value='.$wco_jobs_publication_start.' />'
                . '&nbsp<a>Do: </a><input type="text" id="wco_jobs_publication_end" name="wco_jobs_publication_end" value='.$wco_jobs_publication_end.' />';
        echo '<a id="is_offer_actual"></a>';
}

//save experiance
add_action( 'save_post', 'wco_jobs_save_post' );
function wco_jobs_save_post( $post_id ) {

	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
		return;
        /*
	if ( !wp_verify_nonce( $_POST['wco_jobs_publication_start'], plugin_basename( __FILE__ ) ) )
		return;
	if ( !wp_verify_nonce( $_POST['wco_jobs_publication_end'], plugin_basename( __FILE__ ) ) )
		return;*/

	if ( 'page' == $_POST['post_type'] ) {
		if ( !current_user_can( 'edit_page', $post_id ) )
		return;
	} else {
		if ( !current_user_can( 'edit_post', $post_id ) )
		return;
	}
        
	$wco_jobs_publication_start = $_POST['wco_jobs_publication_start'];
	$wco_jobs_publication_end = $_POST['wco_jobs_publication_end'];
	update_post_meta( $post_id, 'wco_jobs_publication_start', $wco_jobs_publication_start );
	update_post_meta( $post_id, 'wco_jobs_publication_end', $wco_jobs_publication_end );
 	
	$taxonomies = get_object_taxonomies( 'wco_jobs' );

}

add_filter( 'manage_edit-wco_jobs_columns', 'my_edit_wco_jobs_columns' ) ;
function my_edit_wco_jobs_columns( $columns ) {

	$columns = array(
		'cb' => '<input type="checkbox" />',
		'title' => __( 'Oferta' ),
		'from' => __( 'Od' ),
		'to' => __( 'Do' ),
		'displayed' => __( 'Wyświetlana' ),
		'date' => __( 'Date' )
	);

	return $columns;
}

add_action( 'manage_wco_jobs_posts_custom_column', 'my_manage_wco_jobs_columns', 10, 2 );
function my_manage_wco_jobs_columns( $column, $post_id ) {
	global $post;

	switch( $column ) {

		case 'from' :
			$start = get_post_meta( $post_id, 'wco_jobs_publication_start', true );
			if ( empty( $start ))
				echo __( 'Nie ma' );
			else
				printf( __( 'Od <b>%s</b>' ), $start);
			break;
                        
		case 'to' :
                        $end = get_post_meta( $post_id, 'wco_jobs_publication_end', true );
			if ( empty( $end ) )
				echo __( 'Nie ma' );
			else
				printf( __( 'Do <b>%s</b>' ), $end );
			break;

		case 'displayed' :
                        $start = get_post_meta( $post_id, 'wco_jobs_publication_start', true );
			$end = get_post_meta( $post_id, 'wco_jobs_publication_end', true );
                        if(date('Y-m-d', time()) >= get_post_meta( get_the_ID(), 'wco_jobs_publication_start', true )
                        && date('Y-m-d', time()) <= get_post_meta( get_the_ID(), 'wco_jobs_publication_end', true )
                        && get_post_status( get_the_ID()) == "publish" ){
                            echo __( '<span class="status_on">Wyświetla się</span>' );
                        }
                        else
                        {
                            echo __( '<span class="status_off">Nie wyświetla się</span>' );
                        }
                        break;

		/* Just break out of the switch statement for everything else. */
		default :
			break;
	}
}

add_action('wp_head', 'apply_custom_style');
function apply_custom_style() {
//if(is_page())
    if ( get_page_template_slug( get_the_ID() ) == "tpl-wco_jobs_list.php" || 
         get_page_template_slug( get_the_ID() ) == "tpl-wco_jobs_offer_details.php" ){
            //print_r( 'PAGE TEMPLATE: '.get_page_template_slug(get_the_ID()) );
            //print_r('apply_custom_style');
            echo '<style>'
            .get_option('wco_jobs_custom_styles')
            .'</style>';
    }
    else{
         //print_r('NO_apply_custom_style');
    }
}