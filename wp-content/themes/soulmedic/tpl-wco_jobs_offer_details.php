<?php
if ( ! defined( 'ABSPATH' ) ) exit; //session_start();
/**
 * Template Name: RCT Job Post Application Form
 *
 * A template used to demonstrate how to include the template
 * using this plugin.
 *
 * @package PTE
 * @since 	1.0.0
 * @version	1.0.0
 */
?>

<?php echo "<h1 style='font-size:30px; color:red;'></h1>";

wp_enqueue_style('custom_style', plugins_url('/wco-jobs/admin/css/custom_style.css'), false);
wp_enqueue_style('admin_css_bootstrap', plugins_url('/wco-jobs/admin/css/bootstrap.css'), false, '2.3.2', 'all');

//wp_register_script( 'form_submit', plugins_url('/job-post/js/form_submit.js'), array('jquery'));
//wp_enqueue_script('form_submit');
//wp_register_script( 'form_validate', plugins_url('/job-post/js/jquery.validate.min.js'), array('jquery'),'1.9.0');
//wp_enqueue_script('form_validate');

get_header();

//create a captch random number

$ranStr = md5(microtime());
$ranStr = substr($ranStr, 0, 6);
$_SESSION['cap_code'] = $ranStr;
$cap = $_SESSION['cap_code'];

$id_get = null;
$id_get =$_GET['app'];
$args = [
    'post_type' => 'page',
    'fields' => 'ids',
    'nopaging' => true,
    'meta_key' => '_wp_page_template',
    'meta_value' => 'tpl-wco_jobs_list.php'
];
$pages = get_posts( $args );
$pid = $pages[0];

$args=array(
  'post_type' => 'wco_jobs',
  'post_status' => 'publish',
);
//$email = ( get_option('rctjp_smtp_form_email') ? get_option('rctjp_smtp_form_email') : get_option('admin_email'));
?>
<!--<div> <a href="<?php echo get_permalink($pid); ?>"> back to list </a> </div>-->

<div id="simple-msg"></div>
<h2 class="text-center title_post"> Oferta pracy </h2>
<div>
	<h4> Szczegóły oferty! </h4>
</div>
	
    <div>
		<!--<span> Position Applied For : * <span>
		<select name="position_applay" id="position_applay" required>
			<option value="" selected disabled>-- select --</option>
			<?php 
				$my_query = new WP_Query($args);
				while ($my_query->have_posts()) : $my_query->the_post(); 
					  $val = get_the_id();
					?>
					<option value="<?php the_ID(); ?>"  <?php if($id_get == $val){ echo 'selected'; } ?>> <?php the_title(); ?> </option>
					<?php
				endwhile
			?>
		</select>-->
                <h3> <?php echo get_the_title($id_get); ?> </h3>
                <p>
                    <?php 
                                global $post;
                                $post = get_post($id_get);
                                setup_postdata($post);
                                the_content();
                        ?>
                </p>
	</div>
	<div >
            <a href="<?php echo get_permalink($pid); ?>"> <button class="btn btn-info" name="back"> Powrót do ofert </button></a>
	</div>
	
<?php
get_footer();
?>
