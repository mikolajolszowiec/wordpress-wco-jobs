<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://wco.pl
 * @since             1.0.0
 * @package           Wco_Jobs
 *
 * @wordpress-plugin
 * Plugin Name:       WCO Jobs 
 * Plugin URI:        http://example.com/plugin-name-uri/
 * Description:       Job board for WCO.
 * Version:           1.0.0
 * Author:            Mikołaj Olszowiec
 * Author URI:        http://mikolajolszowiec.pl/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       wco-jobs
 * Domain Path:       /languages
 */

// Version
define( 'WCOJOBS_VERSION', '1.0.0' );
define( 'WCOJOBS_DB_VERSION', 1 );

// Define the URL to the plugin folder
define( 'WCOJOBS_FOLDER', 'job-post' );

if( ! defined( 'WCOJOBS_URL' ) )
	define( 'WCOJOBS_URL', WP_PLUGIN_URL . '/' . WCOJOBS_FOLDER );

// Define the basename
define( 'WCOJOBS_BASENAME', plugin_basename( __FILE__ ) );

// Define the complete directory path
define( 'WCOJOBS_DIR', dirname( __FILE__ ) );
define( 'WCOJOBS_DIR_PATH', plugin_dir_path( __FILE__ ) );
 
//const GET_METABOX_ERROR_PARAM = 'meta-error';
 
// global functions
require_once(  WCOJOBS_DIR_PATH . 'function.php' );
require_once(  WCOJOBS_DIR_PATH . 'tables.php' );

// add tempalte
require_once(  WCOJOBS_DIR_PATH . 'tpl-class-page-template.php' );
add_action( 'plugins_loaded', array( 'WCO_Page_Template_Plugin', 'get_instance' ) );
//require_once(  WCOPOST_DIR_PATH . 'js/rctjp_contact_send_mail.php' );
//require_once(  WCOPOST_DIR_PATH . 'templates/rctjp_re_captcha.php' );


// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-plugin-name-activator.php
 */
function activate_wco_jobs() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wco-jobs-activator.php';
	Wco_Jobs_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-plugin-name-deactivator.php
 */
function deactivate_wco_jobs() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wco-jobs-deactivator.php';
	Wco_Jobs_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_wco_jobs' );
register_deactivation_hook( __FILE__, 'deactivate_wco_jobs' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-wco-jobs.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_wco_jobs() {

	$plugin = new Wco_Jobs();
	$plugin->run();

}
run_wco_jobs();


