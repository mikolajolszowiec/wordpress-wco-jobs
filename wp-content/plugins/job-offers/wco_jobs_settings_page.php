<?php if ( ! defined( 'ABSPATH' ) ) exit; ?>
<?php 
if(isset($_POST['settings_save'])){
	
	$wco_jobs_offers_per_page = esc_html( $_POST['wco_jobs_offers_per_page'] );
	$wco_jobs_custom_styles = esc_html( $_POST['wco_jobs_custom_styles'] );
	/*$smtp_port = intval( $_POST['smtp_port'] );
	$smtp_username = sanitize_email( $_POST['smtp_username'] );
	$smtp_password = esc_html( $_POST['smtp_password'] );
	$smtp_form_email = sanitize_email( $_POST['smtp_form_email'] );
	*/
	update_option( 'wco_jobs_offers_per_page', $wco_jobs_offers_per_page);
        update_option( 'wco_jobs_custom_styles', $wco_jobs_custom_styles);
	/*update_option( 'rctjp_smtp_port', $smtp_port);
	update_option( 'rctjp_smtp_username', $smtp_username);
	update_option( 'rctjp_smtp_password', $smtp_password);
	update_option( 'rctjp_smtp_form_email', $smtp_form_email );
	*/
}

$wco_jobs_offers_per_page = get_option('wco_jobs_offers_per_page');
$wco_jobs_custom_styles = get_option('wco_jobs_custom_styles');
//$rctjp_smtp_form_email = ( get_option('rctjp_smtp_form_email') ? get_option('rctjp_smtp_form_email') : get_option('admin_email'));

?> <!--<?php echo __( 'Multisite, now faster than ever', 'wco-jobs' ); ?>-->
<h1><?php echo __( 'Ustawienia' ); ?></h1>
<?php echo __( '(Ilość ofert na stronie, aplikacje itp)', 'wco-jobs'  ); ?>
<form action="" method="post">
	<table class="form-table">
		<tbody>
			<tr class="form-field">
				<th scope="row">
					<label for="user_login"><?php echo __( 'Ilość ofert na stronie:', 'wco-jobs'  ); ?></label>
				</th>
				<td>
					<input name="wco_jobs_offers_per_page" type="number" id="offers_per_page" value=<?php echo $wco_jobs_offers_per_page; ?> aria-required="true" autocapitalize="none" autocorrect="off" maxlength="60">
					<p><span id="footer-thankyou"><?php echo __( '(Na przykład : 20)', 'wco-jobs'  ); ?></span></p>
				</td>
			</tr>
			<tr class="form-field">
				<th scope="row">
					<label for="user_login"><?php echo __( 'Kolejna właściwość do edycji... (Chwilowo nie działa)', 'wco-jobs'  ); ?></label>
				</th>
				<td>
					<input name="unidentified" type="text" id="unidentified" value="dhjyhjrtfsdfbcv" aria-required="true" autocapitalize="none" autocorrect="off" maxlength="10">
					<p><span id="footer-thankyou"><?php echo __( '(Na przykład : dhjyhjrtfsdfbcv)', 'wco-jobs'  ); ?></span></p>
				</td>
			</tr>
			<tr class="form-field">
				<th scope="row">
					<label for="user_login"> Domyślne style w pluginie: </label>
				</th>
				<td>
					<input name="default_styles" type="text" id="default_styles" value="" aria-required="true" autocapitalize="none" autocorrect="off" maxlength="60">
					<p><span id="footer-thankyou">(For Example : style.css)</span></p>
				</td>
			</tr>
                        <tr class="form-field">
				<th scope="row">
					<label for="user_css"> Własne style: </label>
				</th>
				<td>
                                    <textarea name="wco_jobs_custom_styles" type="text" id="custom_styles" class="wco-jobs-textarea"><?php echo $wco_jobs_custom_styles; ?></textarea>
					<p><span id="custom-styles">(For Example : style.css)</span></p>
				</td>
			</tr>
			<tr class="form-field">
				<td> <input type="submit" name="settings_save" value="Save Settings" class="button button-primary"> </td>
			</tr>
		</tbody>
	</table>
</form>