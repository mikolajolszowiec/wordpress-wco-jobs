<?php
if ( ! defined( 'ABSPATH' ) ) exit; //session_start();
/**
 * Template Name: RCT Job Post Application Form
 *
 * A template used to demonstrate how to include the template
 * using this plugin.
 *
 * @package PTE
 * @since 	1.0.0
 * @version	1.0.0
 */
?>

<?php echo "<h1 style='font-size:30px; color:red;'></h1>";

wp_enqueue_style('custom_style', plugins_url('/wco-jobs/admin/css/custom_style.css'), false);
wp_enqueue_style('admin_css_bootstrap', plugins_url('/wco-jobs/admin/css/bootstrap.css'), false, '2.3.2', 'all');

//wp_register_script( 'form_submit', plugins_url('/job-post/js/form_submit.js'), array('jquery'));
//wp_enqueue_script('form_submit');
//wp_register_script( 'form_validate', plugins_url('/job-post/js/jquery.validate.min.js'), array('jquery'),'1.9.0');
//wp_enqueue_script('form_validate');

get_header();

//create a captch random number

$ranStr = md5(microtime());
$ranStr = substr($ranStr, 0, 6);
$_SESSION['cap_code'] = $ranStr;
$cap = $_SESSION['cap_code'];

$id_get = ($_GET['app'])?(int)$_GET['app']:null;
$args = [
    'post_type' => 'page',
    'fields' => 'ids',
    'nopaging' => true,
    'meta_key' => '_wp_page_template',
    'meta_value' => 'tpl-wco_jobs_list.php'
];
$pages = get_posts( $args );
$pid = $pages[0];

$args=array(
  'post_type' => 'wco_jobs',
  'post_status' => 'publish',
);

?>


<div id="simple-msg"></div>
<h2> Oferta pracy </h2>
	
    <div>
		
                <h3> <?php echo get_the_title($id_get); ?> </h3>
                <h1>&nbsp</h1>
                <p>
                    <?php 
                                global $post;
                                $post = get_post($id_get);
                                setup_postdata($post);
                                the_content();
                        ?>
                </p>
	</div>
	<div >
            <a href="<?php echo get_permalink($pid); ?>"> <button class="wco-jobs-p-left wco-jobs-p-button" name="back"> Powrót do ofert </button></a>
	</div>
	
<?php
get_footer();
?>
