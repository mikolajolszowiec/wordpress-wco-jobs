<?php
if ( ! defined( 'ABSPATH' ) ) exit; 
/**
 * Template Name: WCO Jobs List
 *
 * The second template used to demonstrate how to include the template
 * using this plugin.
 *
 * @package PTE
 * @since 	1.0.0
 * @version	1.0.0
 */
?>

<?php
wp_enqueue_style('custom_style', plugins_url('/wco-jobs/admin/css/custom_style.css'), false);
wp_enqueue_style('admin_css_bootstrap', plugins_url('/wco-jobs/admin/css/bootstrap.css'), false, '2.3.2', 'all');

get_header(); 

//temaplte page ids
$args = [
    'post_type' => 'page',//'page',
    'fields' => 'ids',
    'nopaging' => true,
    'meta_key' => '_wp_page_template',
    'meta_value' => 'tpl-wco_jobs_offer_details.php'
];
$pages = get_posts( $args );
foreach ( $pages as $page ) {
     $pid = $page ;
}
///---- for pagination----------/////
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$total_cat = count(get_terms('wco_jobs_category'));
$per_page    = 2;
$total_pages = ceil($total_cat/$per_page);
$offset      = $per_page * ( $paged - 1) ;
$args = array(
        'order'        => 'ASC',
        'orderby'      => 'menu_order',
		'offset'       => $offset,
		'number'	   => $per_page
);
$mystring = get_permalink($pid);
$findme = '?';
$pos = strstr($mystring, $findme);
//...............................List of job post......................................................//
$categories = get_terms('wco_jobs_category', $args);

if ( ! empty( $categories ) ) {
  
	foreach($categories as $cat){
		echo '<h3 class="">' . esc_html( $cat->name ); 
		echo '</h3>';
		$catPost = get_posts(array('post_type' => 'wco_jobs','orderby'=> 'date','order'=> 'DESC','posts_per_page' => -1));
                echo "<table><thead><tr><th>Nazwa oferty/stanowiska</th><th>&nbsp</th></tr></thead><tbody>";
		foreach ($catPost as $post) : setup_postdata($post); ?>
		<?php $cat = get_the_term_list( get_the_ID(), 'wco_jobs_category','', ',','' );
		$pieces = explode(",", strip_tags($cat));
                if(in_array($cat->name,$pieces) && 
                date('Y-m-d', time()) >= get_post_meta( get_the_ID(), 'wco_jobs_publication_start', true ) &&
                date('Y-m-d', time()) <= get_post_meta( get_the_ID(), 'wco_jobs_publication_end', true )){?>
                    <tr>
                        <td><a href="<?php echo get_permalink($pid); ?>&app=<?php the_ID(); ?>" class="wco-jobs-p-label"><?php the_title();?></a></td>
                        <?php 
                        if($pos){ 
                        ?>
                        <td>
                            <a href="<?php echo get_permalink($pid); ?>&app=<?php the_ID(); ?>" class=""><button class="wco-jobs-p-button"> Szczegóły oferty </button></a>
                        </td>
                        <?php 
                        }
                        else
                        { 
                        ?>
                        <td>
                            <a href="<?php echo get_permalink($pid); ?>?app=<?php the_ID(); ?>" class=""><button class="wco-jobs-p-button"> Szczegóły oferty </button></a>
                        </td>
                        <?php
                        }
                        ?>
                    </tr >   
                    <?php
		}
		endforeach;
                echo "</tbody></table>";
		rewind_posts();
		wp_reset_query();
	}
	?>
	<div class="pg_main">
		<div class="wco-jobs-p-left"><?php previous_posts_link('« Poprzednie ') ?> </div>
		<div class="wco-jobs-p-right"><?php next_posts_link('Następne »', $total_pages  ) ?> </div>
	</div>
	<?php
}
else{
	echo '<center><h1>Sorry!!! Nic nie znaleziono</h1><center>';
}
get_footer();ob_flush(); ?>