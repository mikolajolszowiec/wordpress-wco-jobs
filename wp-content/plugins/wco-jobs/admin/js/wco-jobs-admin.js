(function( $ ) {
	'use strict';

})( jQuery );

function getToday(){
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();

    if(dd<10) {
        dd='0'+dd
    } 

    if(mm<10) {
        mm='0'+mm
    } 

    today = yyyy+"-"+mm+'-'+dd;
    return today;
};

function is_offer_actual(){
        if(new Date(jQuery( "#wco_jobs_publication_end" ).attr("value")) < new Date(getToday())
        || new Date(jQuery( "#wco_jobs_publication_start" ).attr("value")) > new Date(getToday())){ 
            jQuery('#is_offer_actual').text("  Ta oferta nie jest wyświetlana");
        }
        else {
            jQuery('#is_offer_actual').text(""); 
        }
}

jQuery(document).ready( function() {
        is_offer_actual();
        console.log(new Date(getToday()));
});

jQuery( function() {
        jQuery( "#wco_jobs_publication_start" ).datepicker({ dateFormat: 'yy-mm-dd', /*maxDate: new Date(jQuery( "#wco_jobs_publication_end" ).attr("value")),*/ onSelect: function () {
        jQuery('#wco_jobs_publication_start').attr("value", this.value);
        is_offer_actual();       
    } }).val();
} );

jQuery( function() {
        jQuery( "#wco_jobs_publication_end" ).datepicker({ dateFormat: 'yy-mm-dd', minDate: new Date(jQuery( "#wco_jobs_publication_start" ).attr("value")), onSelect: function () {
        jQuery('#wco_jobs_publication_end').attr("value", this.value);
        is_offer_actual();        
    } }).val(); 
} );
